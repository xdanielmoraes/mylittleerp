inherited frmCadCategorias: TfrmCadCategorias
  Caption = 'Cadastro de categorias'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pgcPrincipal: TPageControl
    inherited tbListagem: TTabSheet
      inherited dbgDados: TDBGrid
        Columns = <
          item
            Expanded = False
            FieldName = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRIPTION'
            Visible = True
          end>
      end
    end
    inherited tbManutencao: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 24
      ExplicitWidth = 745
      ExplicitHeight = 284
      object editCodigo: TLabeledEdit
        Tag = 2
        Left = 8
        Top = 24
        Width = 121
        Height = 21
        EditLabel.Width = 33
        EditLabel.Height = 13
        EditLabel.Caption = 'C'#243'digo'
        NumbersOnly = True
        TabOrder = 0
      end
      object edtDescricao: TLabeledEdit
        Tag = 1
        Left = 8
        Top = 72
        Width = 523
        Height = 21
        EditLabel.Width = 46
        EditLabel.Height = 13
        EditLabel.Caption = 'Descri'#231#227'o'
        MaxLength = 50
        TabOrder = 1
      end
    end
  end
  inherited pnlRodape: TPanel
    inherited dbNavigator: TDBNavigator
      Hints.Strings = ()
    end
  end
  inherited qryListagem: TZQuery
    SQL.Strings = (
      'SELECT'
      #9'ID,'
      #9'DESCRIPTION'
      'FROM'
      #9'CATEGORY'
      'ORDER BY'
      #9'ID')
    object qryListagemID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      ReadOnly = True
    end
    object qryListagemDESCRIPTION: TWideStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRIPTION'
      Size = 50
    end
  end
end
