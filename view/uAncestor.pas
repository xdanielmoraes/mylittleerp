unit uAncestor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask, Data.DB, Vcl.DBCtrls, Vcl.Grids,
  Vcl.DBGrids, ZAbstractRODataset, ZAbstractDataset, ZDataset, uDmConexao;

type
  TEnumAcao = (actNovo, actAlterar, actNenhuma);
  TfrmAncestor = class(TForm)
    pgcPrincipal: TPageControl;
    pnlRodape: TPanel;
    tbListagem: TTabSheet;
    tbManutencao: TTabSheet;
    pnlListagemTopo: TPanel;
    mskEditPesquisar: TMaskEdit;
    btnPesquisar: TBitBtn;
    dbgDados: TDBGrid;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnCancelar: TBitBtn;
    btnGravar: TBitBtn;
    btnApagar: TBitBtn;
    btnFechar: TBitBtn;
    dbNavigator: TDBNavigator;
    lblPesquisar: TLabel;
    qryListagem: TZQuery;
    dsListagem: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgDadosTitleClick(Column: TColumn);
    procedure mskEditPesquisarChange(Sender: TObject);
    procedure btnApagarClick(Sender: TObject);
  private
    { Private declarations }
    oAcao: TEnumAcao;
    procedure NavegarParaAba(oAba: TEnumAcao);
    procedure DesabilitarCamposPK();
    procedure LimparCamposManutencao;
  public
    { Public declarations }
  protected
    sColumnIndex: String;
    function GetDisplayLabelByFieldName(sFieldName: String): String;
    procedure SortByColumn();
    procedure AlterCaptionPesquisar();
    function Excluir(): Boolean; virtual;
    function Gravar(): Boolean; virtual;
    function ValidarCamposObrigatorios(): Boolean;
  end;

var
  frmAncestor: TfrmAncestor;

implementation

{$R *.dfm}

{$region 'DOCUMENTA��O'}
//Tag 1: Campos obrigat�rios
//Tag 2: Campos de chave prim�ria que devem ser desabilitados
{$endregion}

procedure TfrmAncestor.AlterCaptionPesquisar;
begin
  lblPesquisar.Caption := GetDisplayLabelByFieldName(sColumnIndex);
end;

procedure TfrmAncestor.btnAlterarClick(Sender: TObject);
begin
  oAcao := actAlterar;
  NavegarParaAba(oAcao);
end;

procedure TfrmAncestor.btnApagarClick(Sender: TObject);
begin
  if (Excluir()) then begin
  end;
end;

procedure TfrmAncestor.btnCancelarClick(Sender: TObject);
begin
  oAcao := actNenhuma;
  LimparCamposManutencao();
  NavegarParaAba(oAcao);
end;

procedure TfrmAncestor.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmAncestor.btnGravarClick(Sender: TObject);
begin
  if (ValidarCamposObrigatorios()) then begin
    Abort;
  end;

  if (Gravar()) then begin
    oAcao := actNenhuma;
    NavegarParaAba(oAcao);
    LimparCamposManutencao();
  end;
end;

procedure TfrmAncestor.btnNovoClick(Sender: TObject);
begin
  oAcao := actNovo;
  NavegarParaAba(oAcao);
end;

procedure TfrmAncestor.dbgDadosTitleClick(Column: TColumn);
begin
  sColumnIndex := Column.FieldName;
  SortByColumn();
  AlterCaptionPesquisar();
end;

procedure TfrmAncestor.DesabilitarCamposPK;
var
  iIndexFor: Integer;
begin
  for iIndexFor := 0 to ComponentCount - 1 do begin
    if (Components[iIndexFor] is TLabeledEdit) then begin
      if (TLabeledEdit(Components[iIndexFor]).Tag = 2) then begin
        TLabeledEdit(Components[iIndexFor]).Enabled := False;
      end;
    end;
  end;
end;

procedure TfrmAncestor.LimparCamposManutencao;
var
  iIndexFor: Integer;
begin
  for iIndexFor := 0 to ComponentCount - 1 do begin
    if (Components[iIndexFor] is TLabeledEdit) then begin
      TLabeledEdit(Components[iIndexFor]).Text := EmptyStr;
    end;
  end;
end;

{$region 'VIRTUAL METHODS'}
function TfrmAncestor.Excluir: Boolean;
begin
  Result := True;
end;

function TfrmAncestor.Gravar: Boolean;
begin
  Result := True;
end;
{$endregion}

procedure TfrmAncestor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryListagem.Close;
end;

procedure TfrmAncestor.FormCreate(Sender: TObject);
begin
  qryListagem.Connection := DmConexao.ZConnectionDB;
  dsListagem.DataSet := qryListagem;
  dbgDados.DataSource := dsListagem;
  dbNavigator.DataSource := dsListagem;
  pgcPrincipal.Pages[0].TabVisible := False;
  pgcPrincipal.Pages[1].TabVisible := False;

  oAcao := actNenhuma;
  NavegarParaAba(oAcao);

  dbgDados.Options := [dgTitles,dgIndicator,dgColumnResize,dgColLines,
    dgRowLines,dgTabs,dgRowSelect,dgCancelOnExit,dgTitleClick,
    dgTitleHotTrack,dgAlwaysShowSelection];
end;

procedure TfrmAncestor.FormShow(Sender: TObject);
begin
  if (qryListagem.SQL.Text <> EmptyStr) then begin
    qryListagem.Open;
    SortByColumn();
    AlterCaptionPesquisar();
  end;

  DesabilitarCamposPK();
end;

function TfrmAncestor.GetDisplayLabelByFieldName(sFieldName: String): String;
var
  iIndexFor: Integer;
begin
  for iIndexFor := 0 to qryListagem.Fields.Count - 1 do begin
    if (qryListagem.Fields[iIndexFor].FieldName = sFieldName) then begin
      Result := qryListagem.Fields[iIndexFor].DisplayLabel;
      Break;
    end;
  end;
end;

procedure TfrmAncestor.mskEditPesquisarChange(Sender: TObject);
begin
  qryListagem.Locate(sColumnIndex, TMaskEdit(Sender).Text, [loPartialKey]);
end;

procedure TfrmAncestor.NavegarParaAba(oAba: TEnumAcao);
var
  bFlag: Boolean;
begin
  bFlag := (oAba = actNenhuma);
  btnNovo.Enabled := bFlag;
  btnAlterar.Enabled := bFlag;
  btnCancelar.Enabled := Not(bFlag);
  btnGravar.Enabled := Not(bFlag);
  btnApagar.Enabled := bFlag;
  dbNavigator.Enabled := bFlag;
  if bFlag then
    pgcPrincipal.ActivePage := tbListagem
  else
    pgcPrincipal.ActivePage := tbManutencao;
end;

procedure TfrmAncestor.SortByColumn();
begin
  qryListagem.IndexFieldNames := sColumnIndex;
end;

function TfrmAncestor.ValidarCamposObrigatorios: Boolean;
var
  iIndexFor: Integer;
begin
  Result := False;
  for iIndexFor := 0 to ComponentCount - 1 do begin
    if (Components[iIndexFor] is TLabeledEdit) then begin
      if (TLabeledEdit(Components[iIndexFor]).Tag = 1) and
        (TLabeledEdit(Components[iIndexFor]).Text = EmptyStr) then begin
          MessageDlg(TLabeledEdit(Components[iIndexFor]).EditLabel.Caption +
            ' � um campo obrigat�rio!', mtInformation, [mbOk], 0);
          TLabeledEdit(Components[iIndexFor]).SetFocus;
          Result := True;
          Abort;
      end;
    end;
  end;
end;

end.
