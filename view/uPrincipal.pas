unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, uDmConexao, uCadCategorias, Enter;

type
  TfrmPrincipal = class(TForm)
    menuPrincipal: TMainMenu;
    Cadastro1: TMenuItem;
    Movimentao1: TMenuItem;
    Relatrio1: TMenuItem;
    Cliente1: TMenuItem;
    Categoria1: TMenuItem;
    Produto1: TMenuItem;
    Vendas1: TMenuItem;
    Cliente2: TMenuItem;
    Produto2: TMenuItem;
    Vendas2: TMenuItem;
    N1: TMenuItem;
    Fechar1: TMenuItem;
    procedure Fechar1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Categoria1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    TeclaEnter: TMREnter;
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

procedure TfrmPrincipal.Categoria1Click(Sender: TObject);
begin
  frmCadCategorias := TFrmCadCategorias.Create(Self);
  frmCadCategorias.ShowModal;
  frmCadCategorias.Release;
end;

procedure TfrmPrincipal.Fechar1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(TeclaEnter);
  FreeAndNil(DmConexao);
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  DmConexao := TDmConexao.Create(Self);
  with DmConexao.ZConnectionDB do begin
    LibraryLocation := 'C:\Daniel Moraes\Delphi\MyLitteERP\libraries\ntwdblib.dll';
    Protocol := 'mssql';
    HostName := '.\SQLEXPRESS';
    Port := 1433;
    User := 'sa';
    Password := 'm@ster2020';
    Database := 'MyLittleERP';
    SQLHourGlass := True;
    Connected := True;
  end;

  TeclaEnter := TMREnter.Create(Self);
  TeclaEnter.FocusEnabled := True;
  TeclaEnter.FocusColor := clInfoBk;
end;

end.
