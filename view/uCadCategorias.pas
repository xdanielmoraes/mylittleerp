unit uCadCategorias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uAncestor, Data.DB, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.Mask, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TfrmCadCategorias = class(TfrmAncestor)
    qryListagemID: TIntegerField;
    qryListagemDESCRIPTION: TWideStringField;
    editCodigo: TLabeledEdit;
    edtDescricao: TLabeledEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadCategorias: TfrmCadCategorias;

implementation

{$R *.dfm}

procedure TfrmCadCategorias.FormCreate(Sender: TObject);
begin
  inherited;
  sColumnIndex := 'ID';
end;

end.
